<?php

require_once '../vendor/tpl.php';

$fileName = 'content1.html';
$content = 'Page 1';

if (isset($_GET["cmd"])) {
    if ($_GET["cmd"] === "goToPage2") {
        $fileName = 'content2.html';
        $content = 'Page 2';
    }
} else {
    header('Location: ./index.php?cmd=goToPage1');
}

print renderTemplate('main.html',
    ['fileName' => $fileName, 'content' => $content]);
