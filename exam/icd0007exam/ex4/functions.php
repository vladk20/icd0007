<?php

function standardDeviation($inputList) : float {
    $count = count($inputList);
    $average = array_sum($inputList) / $count;

    $variance = 0.0;
    foreach($inputList as $each) {
        $variance += pow($each - $average, 2);
    }

    return round(sqrt($variance / $count), 2);
}

function getConnectionWithData($dataFile) : PDO {
    $conn = new PDO('sqlite::memory:');
    $statements = explode(';', join('', file($dataFile)));

    foreach ($statements as $statement) {
        $conn->prepare($statement)->execute();
    }

    return $conn;
}
