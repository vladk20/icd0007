<?php

require_once __DIR__ . '/../vendor/php-test-framework/public-api.php';
require_once __DIR__ . '/ex4.php';

function shouldReturnTheCorrectStudents() {

    $studentList = getStudentInfo();

    assertThat(count($studentList), is(2));

    getStudentByName($studentList, 'Alice');

    getStudentByName($studentList, 'Bob');
}

function studentsHaveCorrectStandardDeviationInfo() {

    $studentList = getStudentInfo();

    $alice = getStudentByName($studentList, 'Alice');
    assertThat($alice->sd, is('0.47'));

    $bob = getStudentByName($studentList, 'Bob');
    assertThat($bob->sd, is('0'));
}

#Helpers

function getStudentByName(array $students, string $name) {
    foreach ($students as $student) {
        if ($student->name === $name) {
            return $student;
        }
    }
    throw new RuntimeException('List does not contain student with name ' . $name);
}

stf\runTests(new stf\PointsReporter([
    1 => 5,
    2 => 25]));
