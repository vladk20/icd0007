<?php

require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/Student.php';

function getStudentInfo() : array {

    $conn = getConnectionWithData(__DIR__ . '/data.sql');

    $stmt = $conn->prepare('SELECT name, g.grade AS gr FROM student s LEFT JOIN grade g on g.id = s.id WHERE g.grade IS NOT NULL ORDER BY s.id');

    $stmt->execute();

    $currentName = '';
    $currentStudentGrades = [];
    $result = [];

    foreach ($stmt as $row) {

        $name = $row['name'];

        if ($currentName === '') {
            $currentName = $name;
        }

        if ($name !== $currentName) {
            $result[] = new Student($currentName, standardDeviation($currentStudentGrades));
            $currentName = $name;
            $currentStudentGrades = [];
        }
        $currentStudentGrades[] = intval($row['gr']);

    }

    $result[] = new Student($currentName, standardDeviation($currentStudentGrades));

    return $result;
}