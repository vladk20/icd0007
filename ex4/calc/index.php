<?php

require_once 'functions.php';

$display = $_POST['display'] ?? '';
$cmd = $_POST['cmd'] ?? '';
$number = $_POST['number'] ?? '';

if ($cmd === 'insert') {
    if ($display !== '') {
        if ($display[-1] === '+' && $number[0] === '+') {
            $display .= substr($number, 1);
        } elseif ($display[-1] === '-' && $number[0] === '-') {
            $display[-1] = '+';
            $display .= substr($number, 1);
        } else {
            $display .= $number;
        }
    } else {
        $display .= $number;
    }

} else if ($cmd === 'plus') {
    if ($display !== '') {
        if ($display[-1] !== '+') {
            $display .= '+';
        }
    } else {
        $display .= '+';
    }
} else if ($cmd === 'minus') {
    if ($display !== '') {
        if ($display[-1] !== '-') {
            $display .= '-';
        }
    } else {
        $display .= '-';
    }
} else if ($cmd === 'evaluate') {
    $display = evaluate($display);
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>

<form method="post">
    Display: <input type="text"
                    readonly="readonly"
                    name="display" value="<?= $display ?>" />

    <br /><br />

    Number: <input type="text" name="number" />
    <button type="submit"
            name="cmd"
            id="insert"
            value="insert">Insert</button>
    <br />
    <button type="submit" name="cmd" value="plus">+</button>
    <button type="submit" name="cmd" value="minus">-</button>

    <br /><br />

    <button type="submit" name="cmd" value="evaluate">Evaluate</button>

</form>

</body>
</html>
