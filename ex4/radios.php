<?php

$actualGrade = $_GET['grade'] ?? 3;
$actualGrade = intval($actualGrade)

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<form method="get">

<?php foreach (range(1, 5) as $grade): ?>

    <input type="radio"
           name="grade"
           <?= $grade === $actualGrade ? 'checked' : ''; ?>
           value="<?= $grade ?>" />
    <?= $grade ?>

<?php endforeach; ?>

</form>
</body>
</html>
