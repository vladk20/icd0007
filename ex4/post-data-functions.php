<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

$post = new Post('Title 1', 'Text 1');

savePost($post);

function savePost(Post $post) {

    $lines = file(DATA_FILE);

    $id = $post->id;
    if ($id === '') {
        $id = count($lines) + 1;
    }

    $result = '';
    $added = false;
    foreach ($lines as $line) {
        $exploded = explode(';', trim($line));

        if ($exploded[0] === $id) {
            $line = $id . ';' . urlencode($post->title) . ';' . urlencode($post->text) . PHP_EOL;
            $added = true;
        }
        $result = $result . $line;
    }
    if ($added) {
        file_put_contents(DATA_FILE, $result);
    } else {
        file_put_contents(DATA_FILE, $result . $id . ';' . urlencode($post->title) . ';' . urlencode($post->text) . PHP_EOL);
    }

    return $id;
}

function deletePostById(string $id) : void {
    $lines = file(DATA_FILE);

    $result = '';
    foreach ($lines as $line) {
        $exploded = explode(';', trim($line));

        if ($exploded[0] !== $id) {
            $result = $result . $line;
        }
    }
    file_put_contents(DATA_FILE, $result);
}

function getAllPosts() : array {

    $lines = file(DATA_FILE);

    $result = [];
    foreach ($lines as $line) {
        [$id, $title, $text] = explode(';', trim($line));

        $post = new Post(urldecode($title), urldecode($text));
        $post->id = $id;

        $result[] = $post;
    }

    return $result;
}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}