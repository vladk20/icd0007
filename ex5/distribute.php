<?php

//$sets = distributeToSets([5, 2, 4, 5, 2]);
//
//var_dump($sets);

function distributeToSets(array $input) : array {
    $result = [];
    foreach ($input as $item) {
        if (isset($result[$item])) {
            $result[$item][] = $item;
        } else {
            $result[$item] = [$item];
        }
    }
    return $result;
}
