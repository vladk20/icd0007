<?php

require_once 'connection.php';

$conn = getConnection();

//$stmt = $conn->prepare('INSERT INTO number (num) VALUES (:num)');
//
//foreach (range(1, 100) as $_) {
//    $n = rand(1, 100);
//    $stmt->bindValue(':num', $n);
//    $stmt->execute();
//}

$stmt = $conn->prepare('INSERT INTO contact (name) VALUES (:name)');

$stmt->bindValue(':name', 'Jill');

$stmt->execute();

$lastIndex = $conn->lastInsertId();

$phones = ['1', '2', '3'];

$stmt = $conn->prepare('INSERT INTO phone (contact_id, number) VALUES (:contact_id, :number)');

foreach ($phones as $phone) {
    $stmt->bindValue(':contact_id', $lastIndex);
    $stmt->bindValue(':number', $phone);

    $stmt->execute();
}
