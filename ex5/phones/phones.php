<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/Contact.php';

getContacts();

function getContacts() : array {
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, name, number 
                                FROM contact c LEFT JOIN phone p on c.id = p.contact_id');

    $stmt->execute();

    $contacts = [];

    foreach ($stmt as $row) {
        $id = $row['id'];
        $name = $row['name'];
        $number = $row['number'];

        if (!isset($contacts[$id])) {
            $contacts[$id] = new Contact($id, $name);
        }

        if ($number !== null) {
            $contacts[$id]->addPhone($number);
        }
    }

    return array_values($contacts);
}