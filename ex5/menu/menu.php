<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/MenuItem.php';

//printMenu(getMenu());

function getMenu() : array {

    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, parent_id, name 
                            FROM menu_item ORDER BY id');

    $stmt->execute();

    $tmp = [];
    $result = [];

    foreach ($stmt as $row) {
        $id = $row['id'];
        $parentId = $row['parent_id'];
        $name = $row['name'];

        $tmp[$id] = new MenuItem($id, $name);

        if ($parentId !== null) {
            $tmp[$parentId]->addSubItem($tmp[$id]);
        } else {
            $result[] = $tmp[$id];
        }
    }
    return $result;
}












function printMenu($items, $level = 0) : void {
    $padding = str_repeat(' ', $level * 3);
    foreach ($items as $item) {
        printf("%s%s\n", $padding, $item->name);
        if ($item->subItems) {
            printMenu($item->subItems, $level + 1);
        }
    }
}
