<?php

class OrderLineDao {

    public string $filePath;

    public function __construct($filePath) {
        $this->filePath = $filePath;
    }

    public function getOrderLines() : array {
        $lines = file($this->filePath);

        $orderLines = [];
        foreach ($lines as $line) {

            [$name, $price, $inStock] = explode(';', trim($line));

            $price = floatval($price); // string to float
            $inStock = $inStock === 'true'; // string to boolean

            $orderLine = new OrderLine($name, $price, $inStock);

            $orderLines[] = $orderLine;
        }

        // print list of order line objects
        foreach ($orderLines as $orderLine) {
            printf('name: %s, price: %s; in stock: %s' . PHP_EOL,
                $orderLine->productName,
                $orderLine->price,
                $orderLine->inStock ? 'true' : 'false');
        }

        return $orderLines;
    }
}
