<?php

require_once '../vendor/tpl.php';
require_once 'BookEx.php';
require_once 'AuthorEx.php';

$books = [];

$book = new BookEx('Head First HTML and CSS', 5, false);
$author = new AuthorEx('Elisabeth', 'Robson');
$book->addAuthor($author);
$author = new AuthorEx('Eric', 'Freeman');
$book->addAuthor($author);

$books[] = $book;

$book = new BookEx('Learning Web Design', 4, false);
$author = new AuthorEx('Jennifer', 'Robbins');
$book->addAuthor($author);

$books[] = $book;

$book = new BookEx('Head First Learn to Code', 4, false);
$author = new AuthorEx('Eric', 'Freeman');
$book->addAuthor($author);

$books[] = $book;

$data = [
    'books' => $books,
    'contentPath' => 'list.html'
];

print renderTemplate('tpl/main2.html', $data);
