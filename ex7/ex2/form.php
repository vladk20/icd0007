<?php

require_once '../vendor/tpl.php';
require_once 'BookEx.php';

$errors = ['Pealkiri peab olema 2 kuni 10 märki', 'Hinne peab olema määratud'];
$book = new BookEx('Head First HTML and CSS', 4, true);



$data = [
    'book' => $book,
    'isEditForm' => true,
    'errors' => $errors,
    'contentPath' => 'form.html'
];

print renderTemplate('tpl/main2.html', $data);
