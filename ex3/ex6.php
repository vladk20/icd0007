<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

savePost(new Post('Html', "some text about html"));

function getAllPosts() : array {
    $result = [];

    $lines = file(DATA_FILE);

    foreach ($lines as $line) {
        $line = explode(";", trim($line));

        $result[] = new Post(urldecode($line[0]), urldecode($line[1]));
    }

    return $result;
}

function savePost(Post $post) : void {

    file_put_contents(DATA_FILE, urlencode($post->title) . ";" . urlencode($post->text) . PHP_EOL, FILE_APPEND);

}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}