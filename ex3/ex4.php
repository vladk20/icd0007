<?php

foreach (range(1, 15) as $i) {
    $result = "";
    if ($i % 3 === 0) {
        $result = $result . "Fizz";
    }

    if ($i % 5 === 0) {
        $result = $result . "Buzz";
    }

    if ($result === "") {
        print $i . PHP_EOL;
    } else {
        print $result . PHP_EOL;
    }
}