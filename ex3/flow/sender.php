<?php

$data = $_GET['text'] ?? '';

if (!empty($_GET)) {
    header("Location: receiver.php?text=" . urlencode('Data was: ' . $data));
    exit;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>

<form method="get" action="sender.php">
    <label for="ta">Message:</label>
    <br>
    <textarea id="ta" name="text"></textarea>
    <br>
    <button name="sendButton" type="submit">Send</button>
</form>

</body>
</html>
