<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];


function isInList($list, $elementToBeFound): bool
{
    foreach ($list as $element) {
        if ($elementToBeFound === $element) {
            return true;
        }
    }
    return false;
}
