<?php

$temp = $_POST["temperature"] ?? "";

$message = "";
if (empty($temp)) {
    $message = "<em>Insert temperature</em>";
} else if (is_numeric($temp)) {
    $result = (intval($temp) - 32) / (9/5);
    $message = "<em>$temp decrees in Fahrenheit is $result decrees in Celsius</em>";
} else {
    $message = "<em>Temperature must be an integer</em>";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Fahrenheit to Celsius</title>
</head>
<body>

    <nav>
        <a href="index.html">Celsius to Fahrenheit</a> |
        <a href="f2c.html">Fahrenheit to Celsius</a>
    </nav>

    <main>

        <h3>Fahrenheit to Celsius</h3>

        <?= $message ?>

    </main>

</body>
</html>
