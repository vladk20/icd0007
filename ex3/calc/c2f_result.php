<?php

$temp = $_POST["temperature"] ?? "";

$message = "";
if (empty($temp)) {
    $message = "<em>Insert temperature</em>";
} else if (is_numeric($temp)) {
    $result = intval($temp) * 9 / 5 + 32;
    $message = "<em>$temp decrees in Celsius is $result decrees in Fahrenheit</em>";
} else {
    $message = "<em>Temperature must be an integer</em>";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Celsius to Fahrenheit</title>
</head>
<body>

    <nav>
        <a href="index.html">Celsius to Fahrenheit</a> |
        <a href="f2c.html">Fahrenheit to Celsius</a>
    </nav>

    <main>

        <h3>Celsius to Fahrenheit</h3>

        <?= $message ?>

    </main>

</body>
</html>
