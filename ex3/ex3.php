<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

function getOddNumbers($listOfNumbers) {
    $oddNumbers = [];

    foreach ($listOfNumbers as $number) {
        if ($number % 2 === 1) {
            $oddNumbers[] = $number;
        }
    }
    return $oddNumbers;
}

print_r(getOddNumbers($numbers));