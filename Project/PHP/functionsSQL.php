<?php

require_once 'Book.php';
require_once 'Author.php';

class FunctionsSQL {
    private PDO $connection;

    public function __construct()
    {
        require_once ("connection.php");
        $this->connection = getConnection();
    }

    public function getAuthors(): array
    {
        $stmt = $this->connection->prepare('SELECT id, firstName, lastName, grade FROM authors');
        $stmt->execute();

        $data = [];

        foreach ($stmt as $row) {
            $data[] = new Author($row['firstName'], $row['lastName'], intval($row['grade']), intval($row['id']));
        }

        return $data;
    }

    public function getBooks(): array
    {
        $stmt = $this->connection->prepare('SELECT b.id as id, title, b.grade as grade, isRead, author_id, firstName, lastName, a.grade as authorGrade FROM books b
                                                    LEFT JOIN book_authors ba on b.id = ba.book_id
                                                    LEFT JOIN authors a on ba.author_id = a.id
                                                    ORDER BY b.id, ba.author_id');
        $stmt->execute();

        $data = [];
        $book = null;

        foreach ($stmt as $row) {
            if ($book === null) {
                $book = new Book($row['title'], intval($row['grade']), $row['isRead'] === '1', intval($row['id']));
            }

            if ($book->id === intval($row['id'])) {
                $book->addAuthor(new Author($row['firstName'], $row['lastName'], intval($row['authorGrade']), intval($row['author_id'])));
            } else {
                $data[] = $book;
                $book = new Book($row['title'], intval($row['grade']), $row['isRead'] === '1', intval($row['id']));

                if ($row['author_id'] !== null) {
                    $book->addAuthor(new Author($row['firstName'], $row['lastName'], intval($row['authorGrade']), intval($row['author_id'])));
                }
            }
        }

        if ($book !== null) {
            $data[] = $book;
        }

        return $data;
    }

    public function getAuthorById($idToGet): ?Author
    {
        $stmt = $this->connection->prepare('SELECT id, firstName, lastName, grade FROM authors WHERE id = :id');
        $stmt->bindValue('id', strval($idToGet));
        $stmt->execute();

        foreach ($stmt as $row) {
            return new Author($row['firstName'], $row['lastName'], intval($row['grade']), intval($row['id']));
        }
        return null;
    }

    public function getBookById($idToGet): Book
    {
        $stmt = $this->connection->prepare('SELECT b.id as id, title, b.grade as grade, isRead, author_id, firstName, lastName, a.grade as authorGrade FROM books b
                                                    LEFT JOIN book_authors ba on b.id = ba.book_id
                                                    LEFT JOIN authors a on ba.author_id = a.id
                                                    WHERE b.id = :id
                                                    ORDER BY b.id, ba.author_id');
        $stmt->bindValue('id', strval($idToGet));
        $stmt->execute();

        $book = null;
        $first = true;

        foreach ($stmt as $row) {
            if ($first) {
                $first = false;
                $book = new Book($row['title'], intval($row['grade']), $row['isRead'] === '1', intval($row['id']));
                if ($row['author_id'] !== null) {
                    $book->addAuthor(new Author($row['firstName'], $row['lastName'], intval($row['authorGrade']), intval($row['author_id'])));
                }
            } else {
                $book->addAuthor(new Author($row['firstName'], $row['lastName'], intval($row['authorGrade']), intval($row['author_id'])));
            }
        }
        return $book;
    }

    public function addAuthor(Author $author) {
        $stmt = $this->connection->prepare('INSERT INTO authors VALUES (default, :firstName, :lastName, :grade)');
        $stmt->bindValue('firstName', $author->firstName);
        $stmt->bindValue('lastName', $author->lastName);
        $stmt->bindValue('grade', strval($author->grade));
        $stmt->execute();
    }

    public function addBook(Book $book, $author1, $author2) {
        $stmt = $this->connection->prepare('INSERT INTO books VALUES (default, :title, :grade, :isRead)');
        $stmt->bindValue('title', $book->title);
        $stmt->bindValue('grade', strval($book->grade));
        $stmt->bindValue('isRead', $book->isRead ? '1' : '0');
        $stmt->execute();

        $book_id = $this->connection->lastInsertId();

        $this->addBookAuthors($book_id, $author1, $author2);
    }

    public function delAuthorById($idToDel) {
        $stmt = $this->connection->prepare('DELETE FROM book_authors WHERE author_id = :id');
        $stmt->bindValue('id', strval($idToDel));
        $stmt->execute();

        $stmt = $this->connection->prepare('DELETE FROM authors WHERE id = :id');
        $stmt->bindValue('id', strval($idToDel));
        $stmt->execute();
    }

    public function delBookById($idToDel) {
        $stmt = $this->connection->prepare('DELETE FROM book_authors WHERE book_id = :id');
        $stmt->bindValue('id', strval($idToDel));
        $stmt->execute();

        $stmt = $this->connection->prepare('DELETE FROM books WHERE id = :id');
        $stmt->bindValue('id', strval($idToDel));
        $stmt->execute();
    }

    public function changeAuthorById(Author $author) {
        $stmt = $this->connection->prepare('UPDATE authors SET firstName = :firstName, lastName = :lastName, grade = :grade WHERE id = :id');
        $stmt->bindValue('firstName', $author->firstName);
        $stmt->bindValue('lastName', $author->lastName);
        $stmt->bindValue('grade', strval($author->grade));
        $stmt->bindValue('id', strval($author->id));
        $stmt->execute();
    }

    public function changeBookById(Book $book, $author1, $author2) {
        $stmt = $this->connection->prepare('UPDATE books SET title = :title, grade = :grade, isRead = :isRead WHERE id = :id');
        $stmt->bindValue('title', $book->title);
        $stmt->bindValue('grade', strval($book->grade));

        $stmt->bindValue('isRead', $book->isRead ? '1' : '0');
        $stmt->bindValue('id', strval($book->id));
        $stmt->execute();

        $stmt = $this->connection->prepare('DELETE FROM book_authors WHERE book_id = :id');
        $stmt->bindValue('id', strval($book->id));
        $stmt->execute();

        $this->addBookAuthors($book->id, $author1, $author2);
    }

    public function addBookAuthors($book_id, $author1, $author2) {

        if ($author1 !== -1) {
            $stmt = $this->connection->prepare('INSERT INTO book_authors VALUES (default, :book_id, :author_id)');
            $stmt->bindValue('author_id', strval($author1));
            $stmt->bindValue('book_id', strval($book_id));
            $stmt->execute();
        }

        if ($author2 !== -1) {
            $stmt = $this->connection->prepare('INSERT INTO book_authors VALUES (default, :book_id, :author_id)');
            $stmt->bindValue('author_id', strval($author2));
            $stmt->bindValue('book_id', strval($book_id));
            $stmt->execute();
        }
    }
}