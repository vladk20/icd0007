<?php
require_once 'Author.php';
require_once 'Book.php';

class Functions {
    public string $AUTHORS_FILE = "authors.txt";
    public string $BOOKS_FILE = "books.txt";

    public function __construct($pathToFiles)
    {
        $this->AUTHORS_FILE = $pathToFiles . $this->AUTHORS_FILE;
        $this->BOOKS_FILE = $pathToFiles . $this->BOOKS_FILE;
    }

    private function findEmptyIndex($filePath) {
        $lines = file($filePath);

        $maxIndex = 0;
        foreach ($lines as $line) {
            $exploded = explode(";", $line);
            if (intval($exploded[0]) > $maxIndex) {
                $maxIndex = $exploded[0];
            }
        }
        return $maxIndex + 1;
    }

    public function getAuthors(): array
    {
        $lines = file($this->AUTHORS_FILE);

        $authors = [];
        foreach ($lines as $line) {
            list($id, $firstName, $lastName, $grade) = explode(";", $line);
            $authors[] = new Author(urldecode($firstName), urldecode($lastName), intval(str_replace(PHP_EOL,'',$grade)), intval($id));
        }
        return $authors;
    }

    public function getBooks() {
        $lines = file($this->BOOKS_FILE);

        $books = [];
        foreach ($lines as $line) {
            list($id, $title, $grade, $isRead) = explode(";", $line);
            $books[] = new Book(urldecode($title), intval($grade), str_replace(PHP_EOL,'',$isRead) === 'on', intval($id));
        }
        return $books;
    }

    public function getAuthorById($idToGet) {
        $lines = file($this->AUTHORS_FILE);

        foreach ($lines as $line) {
            $exploded = explode(";", $line);
            if (intval($exploded[0]) === $idToGet) {
                return new Author(urldecode($exploded[1]), urldecode($exploded[2]), intval(str_replace(PHP_EOL,'',$exploded[3])), intval($exploded[0]));
            }
        }
        return null;
    }

    public function getBookById($idToGet) {
        $lines = file($this->BOOKS_FILE);

        foreach ($lines as $line) {
            $exploded = explode(";", $line);
            if (intval($exploded[0]) === $idToGet) {
                return new Book(urldecode($exploded[1]), intval($exploded[2]), str_replace(PHP_EOL,'',$exploded[3]) === 'on', intval($exploded[0]));
            }
        }
        return null;
    }

    public function addAuthor(Author $author) {
        $id = $this->findEmptyIndex($this->AUTHORS_FILE);

        file_put_contents($this->AUTHORS_FILE, $id . ";" . urlencode($author->firstName) . ";" . urlencode($author->lastName) . ";" . $author->grade . PHP_EOL, FILE_APPEND);
    }

    public function addBook(Book $book) {
        $id = $this->findEmptyIndex($this->BOOKS_FILE);

        $isRead = $book->isRead ? 'on' : 'off';
        file_put_contents($this->BOOKS_FILE, $id . ";" . urlencode($book->title) . ";" . $book->grade . ";" . $isRead . PHP_EOL, FILE_APPEND);
    }

    private function delById($idToDel, $path) {
        $lines = file($path);
        $data = "";

        foreach ($lines as $line) {
            $exploded = explode(";", $line);
            if (intval($exploded[0]) !== $idToDel) {
                $data = $data . $line;
            }
        }

        file_put_contents($path, $data);
    }

    public function delAuthorById($idToDel) {
        $this->delById($idToDel, $this->AUTHORS_FILE);
    }

    public function delBookById($idToDel) {
        $this->delById($idToDel, $this->BOOKS_FILE);
    }

    public function changeAuthorById(Author $author) {
        $lines = file($this->AUTHORS_FILE);
        $data = "";

        foreach ($lines as $line) {
            $exploded = explode(";", $line);
            if (intval($exploded[0]) === $author->id) {
                $data = $data . $author->id . ";" . urlencode($author->firstName) . ";" . urlencode($author->lastName) . ";" . $author->grade . PHP_EOL;
            } else {
                $data = $data . $line;
            }
        }

        file_put_contents($this->AUTHORS_FILE, $data);
    }

    public function changeBookById(Book $book) {
        $lines = file($this->BOOKS_FILE);
        $data = "";

        foreach ($lines as $line) {
            $exploded = explode(";", $line);
            if (intval($exploded[0]) === $book->id) {
                $isRead = $book->isRead ? 'on' : 'off';
                $data = $data . $book->id . ";" . urlencode($book->title) . ";" . $book->grade . ";" . $isRead . PHP_EOL;
            } else {
                $data = $data . $line;
            }
        }

        file_put_contents($this->BOOKS_FILE, $data);
    }
}