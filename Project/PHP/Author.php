<?php

class Author {
    public string $firstName;
    public string $lastName;
    public int $grade;
    public int $id;

    public function __construct($firstName, $lastName, $grade, $id = -1) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;
        $this->id = $id;
    }

    public function validate(): array
    {
        $errors = [];
        if (strlen($this->firstName) > 21 || strlen($this->firstName) < 1) {
            $errors[] = "Eesnimi peab olema 1 kuni 21 märki!";
        }
        if (strlen($this->lastName) > 22 || strlen($this->lastName) < 2) {
            $errors[] = "Perekonnanimi peab olema 2 kuni 22 märki!";
        }
        return $errors;
    }
}