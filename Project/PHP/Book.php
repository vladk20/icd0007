<?php

class Book {
    public string $title;
    public int $grade;
    public bool $isRead;
    public array $authors = [];
    public int $id;

    public function __construct($title, $grade, $isRead, $id = -1) {
        $this->title = $title;
        $this->grade = $grade;
        $this->isRead = $isRead;
        $this->id = $id;
    }

    public function addAuthor($author) {
        $this->authors[] = $author;
    }

    public function validate(): array
    {
        $errors = [];
        if (strlen($this->title) > 23 || strlen($this->title) < 3) {
            $errors[] = "Pealkiri peab olema 3 kuni 23 märki!";
        }
        return $errors;
    }

    public function getFirstAuthorId(): int {
        if (empty($this->authors)) {
            return -1;
        }
        return $this->authors[0]->id;
    }

    public function getSecondAuthorId(): int {
        if (count($this->authors) < 2) {
            return -1;
        }
        return $this->authors[1]->id;
    }
}