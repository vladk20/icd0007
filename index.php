<?php

require_once("Project/PHP/functionsSQL.php");
require_once("Project/PHP/Request.php");
require_once("Project/PHP/vendor/tpl.php");
require_once("Project/PHP/Book.php");
require_once("Project/PHP/Author.php");

$functions = new FunctionsSQL();

$request = new Request($_REQUEST);

$cmd = $request->param('cmd') ? $request->param('cmd') : 'book_list';

$data = [];

if ($cmd === 'book_list') {
    $data = [
        'template' => 'book-list.html',
        'books' => $functions->getBooks()
    ];
    if ($request->param('m')) {
        $data['m'] = $request->param('m');
    }
} else if ($cmd === 'author_list') {
    $data = [
        'template' => 'author-list.html',
        'authors' => $functions->getAuthors()
    ];
    if ($request->param('m')) {
        $data['m'] = $request->param('m');
    }
} else if ($cmd === 'add_book') {
    $data = [
        'template' => 'add-book.html',
        'authors' => $functions->getAuthors()
    ];
} else if ($cmd === 'add_author') {
    $data = [
        'template' => 'add-author.html'
    ];
} else if ($cmd === 'add_book_save') {
    $book = new Book($request->param('title'), intval($request->param('grade')), $request->param('isRead') === 'on');
    $errors = $book->validate();
    if (!empty($errors)) {
        $data = [
            'template' => 'add-book.html',
            'errors' => $errors,
            'book' => $book,
            'author1' => $request->param('author1'),
            'author2' => $request->param('author2'),
            'authors' => $functions->getAuthors()
        ];
    } else {
        $functions->addBook($book, is_numeric($request->param('author1')) ? intval($request->param('author1')) : -1, is_numeric($request->param('author2')) ? intval($request->param('author2')) : -1);
        header("Location: ?cmd=book_list&m=saved");
    }
} else if ($cmd === 'add_author_save') {
    $author = new Author($request->param('firstName'), $request->param('lastName'), intval($request->param('grade')));
    $errors = $author->validate();
    if (!empty($errors)) {
        $data = [
            'template' => 'add-author.html',
            'errors' => $errors,
            'author' => $author
        ];
    } else {
        $functions->addAuthor($author);
        header("Location: ?cmd=author_list&m=saved");
    }
} else if ($cmd === 'edit_book') {
    $data = [
        'template' => 'add-book.html',
        'edit' => true,
        'book' => $functions->getBookById($request->param('id')),
        'authors' => $functions->getAuthors()
    ];
} else if ($cmd === 'edit_author') {
    $data = [
        'template' => 'add-author.html',
        'edit' => true,
        'author' => $functions->getAuthorById($request->param('id'))
    ];
} else if ($cmd === 'edit_book_save') {
    $book = new Book($request->param('title'), intval($request->param('grade')), $request->param('isRead') === 'on', intval($request->param('id')));
    $errors = $book->validate();
    if (!empty($errors)) {
        $data = [
            'template' => 'add-book.html',
            'errors' => $errors,
            'book' => $book,
            'author1' => $request->param('author1'),
            'author2' => $request->param('author2'),
            'authors' => $functions->getAuthors(),
            'edit' => true
        ];
    } else {
        $functions->changeBookById($book, is_numeric($request->param('author1')) ? intval($request->param('author1')) : -1, is_numeric($request->param('author2')) ? intval($request->param('author2')) : -1);
        header("Location: ?cmd=book_list&m=updated");
    }
} else if ($cmd === 'edit_author_save') {
    $author = new Author($request->param('firstName'), $request->param('lastName'), intval($request->param('grade')), intval($request->param('id')));
    $errors = $author->validate();
    if (!empty($errors)) {
        $data = [
            'template' => 'add-author.html',
            'errors' => $errors,
            'author' => $author,
            'edit' => true
        ];
    } else {
        $functions->changeAuthorById($author);
        header("Location: ?cmd=author_list&m=updated");
    }
} else if ($cmd === 'delete_book') {
    $functions->delBookById($request->param('id'));
    header("Location: ?cmd=book_list&m=deleted");
} else if ($cmd === 'delete_author') {
    $functions->delAuthorById($request->param('id'));
    header("Location: ?cmd=author_list&m=deleted");
}

if (!empty($data)) {
    if ($data['template'] === 'book-list.html') {
        $data['body'] = 'book-list-page';
    } else if ($data['template'] === 'author-list.html') {
        $data['body'] = 'author-list-page';
    } else if ($data['template'] === 'add-book.html') {
        $data['body'] = 'book-form-page';
    } else if ($data['template'] === 'add-author.html') {
        $data['body'] = 'author-form-page';
    }

    print renderTemplate('Project/tpl/main.html', $data);
}